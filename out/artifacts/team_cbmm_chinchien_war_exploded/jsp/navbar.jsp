
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--todo: this JSP contains the navbar which needs to be included in some pages--%>
<%--the navbar should contain "main page", "user's page","account info", "log in", "login out"--%>
<%--the item we show should base on the user's role--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">WebSiteName</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>

            <%--<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>--%>
                <%--<ul class="dropdown-menu">--%>
                    <%--<li><a href="#">Page 1-1</a></li>--%>
                    <%--<li><a href="#">Page 1-2</a></li>--%>
                    <%--<li><a href="#">Page 1-3</a></li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li><a href="#">Page 2</a></li>--%>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <%--String role = (String)session.getAtrribute("role");--%>
            <%--<c:set var="role" scope="session" value="abc"/>--%>
            <%--<c:if test="${role == abc}">{--%>

                <%--<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Guest </a></li>--%>
                <%--<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>--%>
                <%--<li><a href="#"><span class="glyphicon glyphicon-user"></span> Register</a></li>--%>
            <%--</c:if>--%>
            <%--<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>--%>
            <%--<li><a href="#"><span class="glyphicon glyphicon-user"></span> Register</a></li>--%>

            <%--<c:if test = "login">--%>

            <%--</c:if>--%>
                <c:set var="role" scope="session" value="${user}"/>
                <%--<p>my salary : <c:out value="${salary}"/></p>--%>
                <c:choose>
                    <%--<c:when test="${salary <= 0}">--%>
                        <%--so bad。--%>
                    <%--</c:when>--%>
                    <c:when test="${role == user}">
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">me <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">My blog</a></li>
                                <li><a href="#">My account</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Register</a></li>
                    </c:otherwise>
                </c:choose>


                <%--<c:set var="salary" scope="session" value="${1000*2}"/>--%>
                <%--<c:if test="${salary > 3000}">--%>
                    <%--<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">me <span class="caret"></span></a>--%>
                    <%--<ul class="dropdown-menu">--%>
                    <%--<li><a href="#">My blog</a></li>--%>
                    <%--<li><a href="#">My account</a></li>--%>
                    <%--</ul>--%>
                    <%--</li>--%>
                    <%--<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>--%>
                <%--</c:if>--%>
                <%--<c:otherwise>--%>
                    <%--<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>--%>
                    <%--<li><a href="#"><span class="glyphicon glyphicon-user"></span> Register</a></li>--%>
                <%--</c:otherwise>--%>

        </ul>
    </div>
</nav>

<%--<div class="container">--%>
    <%--<h3>Right Aligned Navbar</h3>--%>
    <%--<p>The .navbar-right class is used to right-align navigation bar buttons.</p>--%>
<%--</div>--%>

</body>
</html>

