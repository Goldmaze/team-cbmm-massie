<%--
  Description: a home page for each user
  ----------------------------------------------------------
  Version  |   Date        |   Created by          |   Description
  v1       |   22/05/2018  |   Chinchien & Massie  |
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>HomePage</title>
    <%--Bootstrap--%>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script>
        function getFullContent(articleId) {
            //ceate a cookie and go to a full text servlet
            document.cookie = "article=" + articleId + "\"";
            // window.location="";
            document.alert("test");
        }
    </script>
    >
</head>
<body>
<div class="container">
    <div class="row">
        test
        <%--user info--%>
        <div class="col-3">
            <img src="${user.avatar}">
            <br>
            ${user.username}
            <br>
            ${user.description}

        </div>
        <%--articles--%>
        <div class="col-9">
            test
            <c:forEach var="article" items="${articles}">
                <section class="article">
                    <p>
                            ${article.title}
                        <br>
                            ${article.content}
                        <br>
                        <button onclick="getFullContent(${article.id})">read more</button>
                    </p>
                </section>
            </c:forEach>
        </div>

    </div>

</div>

</body>
</html>
