<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="col-md-12">
    <c:choose>
        <c:when test="${fn:length(articles) gt 0}">

            <c:forEach var="article" items="${articles}">

                    <%--<div class="panel panel-default">--%>

                            <%--<h3 class="panel-title pull-left"><a href="?article=${article.id}"></a></h3>--%>

                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="..." alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">${article.title}</h5>
                                <p class="card-text">${article.content}</p>
                                <a href="#" class="btn btn-primary">Load Full Article</a>
                            </div>
                        </div>


                        <%--</div>--%>
                        <%--<div class="panel-body">--%>
                            <%--<p>${article.content}</p>--%>
                        <%--</div>--%>
                    </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <p>No articles!</p>
        </c:otherwise>
    </c:choose>

    <!--  A form letting users add new articles. -->
    <%--<div class="panel panel-info">--%>

        <%--<div class="panel-heading">--%>
            <%--<h3 class="panel-title">New Article</h3>--%>
        <%--</div>--%>
        <%--<div class="panel-body">--%>
            <%--<form action="example04" method="POST">--%>
                <%--<div class="form-group">--%>
                    <%--<label for="title">Title</label>--%>
                    <%--<input type="text" id="title" name="title" class="form-control" required>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                    <%--<label for="content">Content</label>--%>
                    <%--<textarea id="content" name="content" class="form-control" rows="10" required></textarea>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                    <%--<button type="submit" class="btn btn-primary">Post</button>--%>
                <%--</div>--%>

                <%--<input type="hidden" name="operation" value="add">--%>
            <%--</form>--%>
        <%--</div>--%>


