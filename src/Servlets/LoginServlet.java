package Servlets;
/**
 * Description: Servlets.LoginServlet servlet:
 * - login checking
 * ----------------------------------------------------------
 * Version  |   Date        |   Created by          |   Description
 * v1       |   21/05/2018  |   Chinchien           |   username & pw checking, create a session attribute "username"
 * v2       |   22/05/2018  |   Chinchien & Massie  |   get & set user's info & articles
 */

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.User;
import DAOs.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class LoginServlet extends HttpServlet {
    //if we get any error msg, set session's attribute "errorMsg" to the message that we want to show up
    //when back to the login page
    private String errorMsg;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //connect to the DB
        try (UserDAO userdao = new UserDAO()) {
            //check PW
            String username = request.getParameter("username");
            String inputPassword = request.getParameter("password");
            String password = userdao.getPassword(username);
            //check if the user exits
            if (password == null) {
                //direct back to the login page, and store an error message in session
                errorMsg = "the user name was incorrect, please try again.";
                request.getSession().setAttribute("errorMsg", errorMsg);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
                dispatcher.forward(request, response);
            }
            //check if the password exits: the input pw need to match the hashed pw
            if (password != null) {
                byte[] bytecode = Passwords.base64Decode(password);
                boolean isMatched = Passwords.isInsecureHashMatch(inputPassword, bytecode);
                if (!isMatched) {
                    errorMsg = "The password was incorrect, please try again.";
                    request.getSession().setAttribute("errorMsg", errorMsg);
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
                    dispatcher.forward(request, response);
                }

                request.getSession().setAttribute("username", username);
                /** v2: before we go the the home page, we may need to get the user's info & articles */
                User user = userdao.getAllUserInfoByName(username);
                List<Article> articles = getArticlesByName(username);
                request.getSession().setAttribute("user", user);
                request.getSession().setAttribute("articles", articles);
                request.setAttribute("user", user);
                request.setAttribute("articles", articles);
                //direct the the home page
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            errorMsg = "Our system is not available at the moment, please try again later.";
            request.getSession().setAttribute("errorMsg", errorMsg);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            errorMsg = "Our system is not available at the moment, please try again later.";
            request.getSession().setAttribute("errorMsg", errorMsg);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /** v2 */
    protected List<Article> getArticlesByName(String username) {
        List<Article> articles = null;
        try (ArticleDAO articleDAO = new ArticleDAO()) {
            articles = articleDAO.getArticlesByName(username);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return articles;
    }

}
