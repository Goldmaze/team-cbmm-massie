package DAOs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements AutoCloseable {
    private final Connection conn;


    public UserDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods

    public User getAllUserInfoByName(String username) throws SQLException {
        User user = new User();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_users WHERE username = ?")) {
            stmt.setString(1, username);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    user.setUserId(r.getInt(1));
                    user.setFname(r.getString(2));
                    user.setLname(r.getString(3));
                    user.setUsername(r.getString(4));
                    user.setBirthday(r.getString(5));
                    user.setCountry(r.getString(6));
                    user.setAvatar(r.getString(7));
                    user.setDescription(r.getString(8));
                    user.setFname(r.getString(9));
                }
                return user;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public String getPassword(String username) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT DISTINCT hash_code " +
                "FROM blog_password AS p " +
                "JOIN blog_users AS u " +
                "ON p.id = u.id " +
                "WHERE u.user_name = ?")) {
            stmt.setString(1, username);
            try (ResultSet r = stmt.executeQuery()) {
                if (r.next()) {
                    return r.getString(1);
                } else {
                    return null;
                }
            }
        }
    }

    @Override
    public void close() throws Exception {
        this.conn.close();
    }

    public void createNewUser(User user) throws SQLException{
        try {

            PreparedStatement stmt = conn.prepareStatement("INSERT INTO blog_users VALUES (?,?,?,?,?,?,?,?)");
            {
                stmt.setString(1, user.getFname());
                stmt.setString(2, user.getLname());
                stmt.setString(3, user.getRole());
                stmt.setString(4, user.getUsername());
                stmt.setString(5, user.getBirthday());
                stmt.setString(6, user.getCountry());
                stmt.setString(7, user.getDescription());
//                    stmt.setBlob(8, );
            }

        } catch (SQLException e) {
            e.getMessage();
        }


    }


}
