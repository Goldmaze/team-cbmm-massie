package DAOs;

import com.zaxxer.hikari.HikariDataSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class HikariConnectionPool {
    private static HikariDataSource hds;

    static {
        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        hds = new HikariDataSource();
        hds.setJdbcUrl(dbProps.getProperty("url"));
        hds.setDriverClassName("com.mysql.jdbc.Driver");
        hds.setUsername(dbProps.getProperty("user"));
        hds.setPassword(dbProps.getProperty("password"));
        hds.setMaximumPoolSize(2);
    }

    public static Connection getConnection() throws SQLException {
        return hds.getConnection();
    }
}